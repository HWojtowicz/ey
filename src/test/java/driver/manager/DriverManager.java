package driver.manager;

import configuration.WebDriverProperties;
import org.openqa.selenium.WebDriver;

public class DriverManager {
    private static WebDriver driver;


    private DriverManager() {

    }

    public static WebDriver getWebDriver() {

        if (driver == null) {
            driver = BrowserFactory.getBrowser(WebDriverProperties.getLocalBrowser());
        }
        return driver;
    }

    public static void disposeDriver() {
        driver.close();
        if (!WebDriverProperties.getLocalBrowser().equals(BrowserType.FIREFOX)) {
            driver.quit();
        }
        driver = null;
    }
}
