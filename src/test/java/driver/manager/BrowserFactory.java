package driver.manager;

import configuration.WebDriverProperties;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserFactory {
    public static WebDriver getBrowser(BrowserType browserType) {
        switch (browserType) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
               // System.setProperty("webdriver.chrome.driver", WebDriverProperties.getChromeWebDriverLocation());
                return new ChromeDriver();
            case FIREFOX:
               // System.setProperty("webdriver.firefox.driver", WebDriverProperties.getFirefoxWebDriverLocation());
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver();
            case IE:
              //  System.setProperty("webdriver.ie.driver", WebDriverProperties.getInternetExplorerWebDriverLocation());
                WebDriverManager.iedriver().setup();

                return new InternetExplorerDriver();
            default:
                throw new IllegalStateException("No such driver! Please check configuration");

        }
    }
}

