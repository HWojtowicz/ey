package driver.manager;

public enum BrowserType {
    CHROME("chrome"),
    FIREFOX("firefox"),
    IE("internet explorer");

    private final String browser;

    BrowserType(String browser) {
        this.browser = browser;
    }
}