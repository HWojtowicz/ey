package driver.manager;

import io.qameta.allure.Step;

import java.util.concurrent.TimeUnit;

public class DriverUtils {
    public static void setInitialConfiguration() {
        DriverManager.getWebDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        DriverManager.getWebDriver().manage().window().maximize();
    }


    @Step("Navigate to {pageURL}")
    public static void navigateToPage(String pageURL) {
        DriverManager.getWebDriver().navigate().to(pageURL);
    }
}
