package tests;

import com.github.javafaker.Faker;
import driver.manager.DriverUtils;
import io.qameta.allure.Description;
import navigation.NavigationURLs;
import org.testng.annotations.Test;
import page.objects.LandingPage;
import page.objects.MyAccountPage;
import utils.ScreenShotMaker;

public class CreateNewAccountTest extends TestBase{



    @Test
    @Description("E2E test for creation an account in the portal- positive scenario")
    public void newAccountRegistrationPositiveTest() {
        Faker faker = new Faker();
        String email = faker.internet().emailAddress();
        DriverUtils.navigateToPage(NavigationURLs.APP_URL);
        LandingPage landingPage = new LandingPage();
        landingPage
                .verifyLogIsDisplayed()
                .clickOnSignInButton()
                .verifyPageHeadingOnSignInPage("AUTHENTICATION")
                .enterEmailAddress(email)
                .clickCreateAnAccountButton()
                .verifyCreateAnAccountPageHeader("CREATE AN ACCOUNT")
                .selectMrTitle()
                .enterFirstName()
                .enterLastName()
                .verifyEmail(email)
                .providePassword()
                .selectDayOfBirth()
                .selectMonthOfBirth()
                .selectYearOfBirth()
                .enterStreetAddress()
                .enterCityName()
                .selectState()
                .enterPostalCode()
                .enterMobilePhone()
                .clickRegisterButton();
        MyAccountPage myAccountPage = new MyAccountPage();
        myAccountPage
                .verifyPageMyAccountPageHeader("MY ACCOUNT");
        ScreenShotMaker.makeScreenShot();
    }

    @Test
    public void newAccountRegistrationValidationError(){
        Faker faker = new Faker();
        String email = faker.internet().emailAddress();
        DriverUtils.navigateToPage(NavigationURLs.APP_URL);
        LandingPage landingPage = new LandingPage();
        landingPage
                .verifyLogIsDisplayed()
                .clickOnSignInButton()
                .verifyPageHeadingOnSignInPage("AUTHENTICATION")
                .enterEmailAddress(email)
                .clickCreateAnAccountButton()
                .verifyCreateAnAccountPageHeader("CREATE AN ACCOUNT")
                .selectMrTitle()
                .enterFirstName()
                .enterLastName()
                .verifyEmail(email)
                .provideNotValidPassword()
                .selectDayOfBirth()
                .selectMonthOfBirth()
                .selectYearOfBirth()
                .enterStreetAddress()
                .enterCityName()
                .selectState()
                .enterPostalCode()
                .enterMobilePhone()
                .clickRegisterButton()
                .validationErrorDisplayed();
        ScreenShotMaker.makeScreenShot();
    }
}
