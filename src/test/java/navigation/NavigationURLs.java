package navigation;

import configuration.AppProperties;

public class NavigationURLs {
    public static final String APP_URL = AppProperties.getAllUrl();
}
