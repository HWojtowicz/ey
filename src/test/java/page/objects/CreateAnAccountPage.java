package page.objects;

import assertions.AssertWebElement;
import com.github.javafaker.Faker;
import driver.manager.DriverManager;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.apache.commons.lang3.StringUtils;
import waits.WaitForElements;

import java.util.List;
import java.util.Locale;
import java.util.Random;


public class CreateAnAccountPage {

    //Locale locale = new Locale("ms_MY");
    Faker faker = new Faker();
    String firstName = faker.name().firstName();
    String lastName = faker.name().lastName();
    String passwordPositive = faker.internet().password(5,8);
    String wrongPassword = faker.internet().password(2,4);
    String streetName = faker.address().streetAddress();
    String city = faker.address().cityName();
    String zipCode = StringUtils.left(faker.address().zipCode(), 5);
    String mobilePhone = faker.phoneNumber().cellPhone();


    private Logger logger = LogManager.getRootLogger();

    private static final String title = "//div[@class='account_creation']/div[1]";

    @FindBy(xpath = "//div[@id='noSlide']/h1")
    private WebElement pageHeading;

    @FindBy(css = "#id_gender1")
    private WebElement mrRadioButton;

    @FindBy(xpath = title + "/div[2]/label/div/span/input")
    private WebElement mrsRadioButton;

    @FindBy(css = "#customer_firstname")
    private WebElement firstNameInput;

    @FindBy(css = "#customer_lastname")
    private WebElement lastNameInput;

    @FindBy(css = "#email")
    private WebElement emailProvidedInput;

    @FindBy(css = "#passwd")
    private WebElement passwordInput;

    @FindBy(css = "#days")
    private WebElement dayBirth;

    @FindBy(xpath = "//div[@id='uniform-days']/span")
    private WebElement dayText;

    @FindBy(css = "#months")
    private WebElement monthBirth;

    @FindBy(xpath = "//div[@id='uniform-months']/span")
    private WebElement monthText;

    @FindBy(css = "#years")
    private WebElement yearBirth;

    @FindBy(xpath = "//div[@id='uniform-years']/span")
    private WebElement yearText;

    @FindBy(xpath = "//p[@class='required form-group']/input[@id='firstname']")
    private WebElement firstNameAddressInput;

    @FindBy(xpath = "//p[@class='required form-group']/input[@id='lastname']")
    private WebElement lastNameAddressInput;

    @FindBy(xpath = "//p[@class='required form-group']/input[@id='address1']")
    private WebElement addressInput;

    @FindBy(xpath = "//p[@class='required form-group']/input[@id='city']")
    private WebElement cityInput;

    @FindBy(css = "#id_state")
    private WebElement stateDropDown;

    @FindBy(xpath = "//div[@id='uniform-id_state']/span")
    private WebElement stateText;

    @FindBy(css = "#postcode")
    private WebElement postCodeInput;

    @FindBy(css = "#phone_mobile")
    private WebElement mobilePhoneInput;

    @FindBy(css = "#submitAccount")
    private WebElement registerButton;

    @FindBy(xpath = "//div[@class='alert alert-danger']")
    private WebElement validationError;

    @FindBy(xpath = "//div[@class='alert alert-danger']/ol/li")
    private WebElement validationErrorText;

    public CreateAnAccountPage(){
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @Step("Verify page Heading is: {pageHeadingText}")
    public  CreateAnAccountPage verifyCreateAnAccountPageHeader(String pageHeadingText){
        logger.info("Verify page heading is {}", pageHeadingText);
        WaitForElements.waitUntilElementIsClickable(mrRadioButton);
        WaitForElements.waitUntilElementIsVisible(pageHeading);
        AssertWebElement.assertThat(pageHeading).hasText(pageHeadingText);
        return this;
    }

    @Step("Select title")
    public CreateAnAccountPage selectMrTitle(){
        logger.info("Select title Mr");
        WaitForElements.waitUntilElementIsClickable(mrRadioButton);
        mrRadioButton.click();
        return this;
    }

    @Step("Enter first name")
    public CreateAnAccountPage enterFirstName(){
        firstNameInput.sendKeys(firstName);
        logger.info("Entered first name {}", firstName);
        AssertWebElement.assertThat(firstNameAddressInput).hasExpectedText(firstName);
        return this;
    }

    @Step("Enter last name")
    public CreateAnAccountPage enterLastName(){
        lastNameInput.sendKeys(lastName);
        logger.info("Entered last name {}",lastName);
        AssertWebElement.assertThat(lastNameAddressInput).hasExpectedText(lastName);
        return this;
    }

    @Step("Verify email")
    public CreateAnAccountPage verifyEmail(String providedEmail){
       AssertWebElement.assertThat(emailProvidedInput).hasExpectedText(providedEmail);
       logger.info("Verify that email is correct {}", providedEmail);
        return this;
    }


    @Step("Enter valid password")
    public CreateAnAccountPage providePassword(){
        logger.info("Entered password {}", passwordPositive);
        passwordInput.sendKeys(passwordPositive);
        return this;
    }


    @Step("Enter shorter password")
    public CreateAnAccountPage provideNotValidPassword(){
        logger.info("Enter not valid password {}", wrongPassword);
        passwordInput.sendKeys(wrongPassword);
        return this;
    }



    @Step("Select day of birth")
    public CreateAnAccountPage selectDayOfBirth(){
        Select select = new Select(dayBirth);
        List <WebElement> dayList = select.getOptions();
        int iCnt = dayList.size();
        Random num = new Random();
        int iSelect = num.nextInt(iCnt);
        select.selectByIndex(iSelect);
        String selectedDay = dayText.getText();
        logger.info("Selected day {}", selectedDay);
        return this;
    }

    @Step("Select month of birth")
    public CreateAnAccountPage selectMonthOfBirth(){
        Select select = new Select(monthBirth);
        List <WebElement> monthList = select.getOptions();
        int iCnt = monthList.size();
        Random num = new Random();
        int iSelect = num.nextInt(iCnt);
        select.selectByIndex(iSelect);
        String selectedMonth = monthText.getText();
        logger.info("Selected month {}", selectedMonth);
        return this;
    }

    @Step("Select year of birth")
    public CreateAnAccountPage selectYearOfBirth(){
        Select select = new Select(yearBirth);
        List <WebElement> yearList = select.getOptions();
        int iCnt = yearList.size();
        Random num = new Random();
        int iSelect = num.nextInt(iCnt);
        select.selectByIndex(iSelect);
        String selectedYear = yearText.getText();
        logger.info("Selected year {}", selectedYear);
        return this;
    }

    @Step("Enter Street address")
    public CreateAnAccountPage enterStreetAddress(){
        addressInput.sendKeys(streetName);
        logger.info("Entered street address {}", streetName);
        return this;
    }

    @Step("Enter City")
    public CreateAnAccountPage enterCityName(){
        logger.info("Entered City name {}", city);
        cityInput.sendKeys(city);
        return this;
    }

    @Step("Select State from drop down list")
    public CreateAnAccountPage selectState(){
        Select select = new Select(stateDropDown);
        List <WebElement> stateList = select.getOptions();
        int iCnt = stateList.size();
        Random num = new Random();
        int iSelect = num.nextInt(iCnt);
        select.selectByIndex(iSelect);
        String selectedState= stateText.getText();
        logger.info("Selected state {}", selectedState);
        return this;
    }

    @Step("Enter Postal Code")
    public CreateAnAccountPage enterPostalCode(){
        logger.info("Entered Postal Code {}", zipCode);
        postCodeInput.sendKeys(zipCode);
        return this;
    }

    @Step("Enter mobile phone")
    public CreateAnAccountPage enterMobilePhone(){
        logger.info("Entered phone number {}", mobilePhone);
        mobilePhoneInput.sendKeys(mobilePhone);
        return this;
    }

    @Step("Click on Register button")
    public CreateAnAccountPage clickRegisterButton() {
        logger.info("Clicking register button");
        registerButton.click();
        return this;
    }

    @Step("Verify that validation message displayed")
    public CreateAnAccountPage validationErrorDisplayed(){

        WaitForElements.waitUntilElementIsVisible(validationError);
        String errorMsg = validationErrorText.getText();
        errorMsg.replaceAll("\\r\\n|\\r|\\n", " ");
        Assert.assertEquals(errorMsg, "passwd is invalid.");
        logger.info("Verify that validation error message displayed {}", errorMsg);
        return this;
    }



}
