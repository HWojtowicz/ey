package page.objects;

import driver.manager.DriverManager;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import waits.WaitForElements;

public class LandingPage {
    private Logger logger = LogManager.getRootLogger();

    @FindBy(css = ".logo")
    private WebElement logo;

    @FindBy(css = ".login")
    private WebElement signInButton;


    public LandingPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @Step("Verify that logo is displayed on Landing Page")
    public LandingPage verifyLogIsDisplayed(){

        logger.info("Waiting for Logo diaplyed/clickable");
        WaitForElements.waitUntilElementIsClickable(logo);
        return this;
    }

    @Step("Click on Sign In button")
    public SignInPage clickOnSignInButton(){

        logger.info("Click on Sign in button");
        WaitForElements.waitUntilElementIsClickable(signInButton);
        signInButton.click();
        return new SignInPage();
    }



}
