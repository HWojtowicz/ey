package page.objects;

import assertions.AssertWebElement;
import com.github.javafaker.Faker;
import driver.manager.DriverManager;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElements;

import java.util.Locale;

public class SignInPage {

   // Locale locale = new Locale("ms_MY");
    Faker faker = new Faker();
    String email = faker.internet().emailAddress();

    private Logger logger = LogManager.getRootLogger();


    @FindBy(xpath = "//div[@id='center_column']/h1")
    private WebElement pageHeading;

    @FindBy(css = "#email_create")
    private  WebElement emailToCreateAccount;


    @FindBy(css = "#SubmitCreate")
    private WebElement createAnAccountButton;

    public SignInPage(){
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    @Step("Verify page Heading is: {pageHeadingText}")
    public SignInPage verifyPageHeadingOnSignInPage(String pageHeadingText){

        logger.info("Verify page heading is {}", pageHeadingText);
        WaitForElements.waitUntilElementIsVisible(pageHeading);
        AssertWebElement.assertThat(pageHeading).hasText(pageHeadingText);
        return this;
    }

    @Step("Provided email address")
    public SignInPage  enterEmailAddress(String emailAddress){
        emailToCreateAccount.sendKeys(emailAddress);
        logger.info("Entered email address {}", emailAddress);
        return this;
    }


    @Step("Click Create an account button")
    public CreateAnAccountPage clickCreateAnAccountButton(){
        logger.info("Click Create an account button");
        WaitForElements.waitUntilElementIsClickable(createAnAccountButton);
        createAnAccountButton.click();
        return new CreateAnAccountPage();
    }
}
