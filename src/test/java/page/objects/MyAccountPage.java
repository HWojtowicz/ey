package page.objects;

import assertions.AssertWebElement;
import driver.manager.DriverManager;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElements;

public class MyAccountPage {

    private Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//div[@id='center_column']/h1")
    private WebElement pageHeading;

    @FindBy(css = ".myaccount-link-list")
    private WebElement myAccountList;

    public MyAccountPage(){
        PageFactory.initElements(DriverManager.getWebDriver(), this);

    }

    @Step("Verify page header")
    public MyAccountPage verifyPageMyAccountPageHeader(String myAccountPageHeader){
        logger.info("Verify page heading is {}", myAccountPageHeader);
        WaitForElements.waitUntilElementIsVisible(myAccountList);
        AssertWebElement.assertThat(pageHeading).hasText(myAccountPageHeader);
        return this;
    }


}
