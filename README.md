**Requirements**
Chrome browser ver 89
mvn installed 

**Steps to run automated test case**

- Clone repository to your local machine ( detail description: https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/cloning-a-repository)
- Open cmd and navigate to the main folder of cloned project
- Execute command mvn clean test
- After test execution completion run command  mvn allure:serve



_Raport should be open in browser with detailed  information about the execution. _
